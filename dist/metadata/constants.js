"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DESIGN_PARAM_TYPES = "design:paramtypes";
exports.DI_INJECTABLE = "bilbercode:injectable";
exports.INJECTABLE = "bilbercode:injectable:options";
exports.FACTORY = "bilbercode:factory";
exports.FACTORY_OPTIONS = "bilbercode:factory:options";
exports.CONTROLLER = "bilbercode:controller";
exports.CONTROLLER_ROUTE = "bilbercode:controller:route";
exports.AMQP_EXCHANGE = "bilbercode:amqp:exchange";
exports.AMQP_QUEUE = "bilbercode:amqp:queue";
exports.FORM = "bilbercode:form";
exports.INPUT = "bilbercode:input";
exports.FORM_INPUT = "bilbercode:form:input";
exports.ARGUMENT = "bilbercode:argument";
exports.ARGUMENT_BODY = Symbol("body");
exports.ARGUMENT_ROUTE = Symbol("route");
exports.ARGUMENT_HEADER = Symbol("header");
exports.ARGUMENT_CONTEXT = Symbol("context");
exports.ARGUMENT_ADDON = Symbol("addon");
exports.ARGUMENT_TIMER = Symbol("timer");
exports.ARGUMENT_PROPERTY = Symbol("property");
exports.APPLICATION_MODULE = "bilbercode:application:module";
exports.APPLICATION_MODULE_CLASS = "bilbercode:application:module:class";
exports.MODULE_SERVICE_LOCATOR = Symbol("module-service-locator");
exports.SHARED_SERVICE = "bilbercode:sharedservice";
//# sourceMappingURL=constants.js.map