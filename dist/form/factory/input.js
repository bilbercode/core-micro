"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var input_1 = require("../input/input");
var InputFactory = (function () {
    function InputFactory() {
    }
    InputFactory.prototype.createService = function (serviceLocator, options) {
        return new input_1.Input(options.source, options.property, options.inputName, this.locate(serviceLocator, options.filters || []), this.locate(serviceLocator, options.validators || []));
    };
    InputFactory.prototype.locate = function (serviceLocator, items) {
        return items.map(function (ITEM) {
            if (serviceLocator.has(ITEM)) {
                return serviceLocator.get(ITEM);
            }
            return ITEM;
        });
    };
    return InputFactory;
}());
exports.InputFactory = InputFactory;
//# sourceMappingURL=input.js.map