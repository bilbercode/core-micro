"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var util_1 = require("util");
var injectable_1 = require("../../../decorator/injectable");
var NotEmpty = (function () {
    function NotEmpty() {
        this.name = "NotEmpty";
    }
    NotEmpty.prototype.validate = function (value) {
        if (util_1.isNullOrUndefined(value) || String(value) === "") {
            return "Required";
        }
        return true;
    };
    return NotEmpty;
}());
NotEmpty = __decorate([
    injectable_1.Injectable()
], NotEmpty);
exports.NotEmpty = NotEmpty;
//# sourceMappingURL=not-empty.js.map