"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Response = (function () {
    function Response(input, raw, clean, messages, isValid) {
        this.input = input;
        this.raw = raw;
        this.clean = clean;
        this.messages = messages;
        this.isValid = isValid;
    }
    return Response;
}());
exports.Response = Response;
//# sourceMappingURL=response.js.map