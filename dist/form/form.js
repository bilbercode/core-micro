"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var input_1 = require("./input/input");
var Form = (function () {
    function Form(boundInstance) {
        this.inputs = [];
        this.boundInstance = boundInstance;
    }
    Form.prototype.isValid = function (requestContext) {
        var _this = this;
        var inputContext = {};
        this.inputs.forEach(function (INPUT) {
            inputContext[INPUT.property] = _this.getContext(requestContext, INPUT.source, INPUT.inputName);
        });
        var VALIDATION_CHAIN = this.inputs
            .map(function (INPUT) { return INPUT.isValid(_this.getContext(requestContext, INPUT.source, INPUT.inputName), inputContext); });
        return Promise.all(VALIDATION_CHAIN)
            .then(function (RESULTS) {
            var valid = true;
            var messages = {};
            RESULTS.forEach(function (RESULT) {
                if (!RESULT.isValid) {
                    valid = false;
                    messages[RESULT.input.inputName] = RESULT.messages;
                }
            });
            if (valid) {
                RESULTS.forEach(function (RESULT) {
                    _this.boundInstance[RESULT.input.property] = RESULT.clean;
                });
                return true;
            }
            return messages;
        });
    };
    Form.prototype.getObject = function () {
        return this.boundInstance;
    };
    Form.prototype.getContext = function (requestContext, inputContext, inputName) {
        switch (inputContext) {
            case input_1.EInputSource.bodyParam:
                return requestContext.getBodyParam(inputName);
            case input_1.EInputSource.propertyParam:
                return requestContext.getPropertyParam(inputName);
            default:
                return requestContext.getHeaderParam(inputName);
        }
    };
    return Form;
}());
exports.Form = Form;
//# sourceMappingURL=form.js.map