"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("source-map-support/register");
var injector_1 = require("../di/injector");
/**
 * Loop counter used to detect cyclic dependencies between the DI Injector and service locator
 * @type {WeakMap<symbol, number>}
 */
var LOOP_DETECTOR = new Map();
function isRequestingSelf(value) {
    if (!!(value === ServiceManager)) {
        return true;
    }
    return false;
}
var ServiceManager = (function () {
    function ServiceManager(metadata) {
        this.metadata = metadata;
        this.instances = new Map();
        this.factories = new Map();
        this.shared = new Map();
        this.peeringManagers = [];
        this.di = new injector_1.Injector(this, metadata);
        this.id = Symbol("ServiceManager");
    }
    Object.defineProperty(ServiceManager.prototype, "peeringManager", {
        get: function () {
            return this._peeringManager;
        },
        enumerable: true,
        configurable: true
    });
    ServiceManager.prototype.has = function (target, ignoreDI) {
        if (ignoreDI === void 0) { ignoreDI = false; }
        if (isRequestingSelf(target)) {
            return true;
        }
        var loopCounter = arguments[3];
        if (!loopCounter) {
            loopCounter = Symbol("LOOP_COUNTER");
            LOOP_DETECTOR.set(loopCounter, 0);
        }
        else {
            LOOP_DETECTOR.set(loopCounter, LOOP_DETECTOR.get(loopCounter) + 1);
        }
        if (LOOP_DETECTOR.get(loopCounter) > 99) {
            LOOP_DETECTOR.delete(loopCounter);
            throw new Error("Cyclic dependency detected!");
        }
        /**
         * Check if we already have a shared instance available ;)
         */
        if (this.instances.has(target)) {
            LOOP_DETECTOR.delete(loopCounter);
            return true;
        }
        /**
         * Check with the peering service first
         */
        if (this.peeringManager && this.peeringManager.has(target)) {
            LOOP_DETECTOR.delete(loopCounter);
            return true;
        }
        if (this.hasPeeringManager()) {
            for (var i = 0; i < this.peeringManagers.length; i++) {
                var PM_HAS = this.peeringManagers[i]
                    .has.call(this.peeringManagers[i], target, ignoreDI, loopCounter);
                if (PM_HAS) {
                    LOOP_DETECTOR.delete(loopCounter);
                    return true;
                }
            }
        }
        /**
         * Check if we have a factory registered for it
         */
        if (this.factories.has(target)) {
            LOOP_DETECTOR.delete(loopCounter);
            return true;
        }
        /**
         * Last ditch check if the DI can instantiate
         * the thing
         */
        if (!ignoreDI && this.di.canInstantiate(target, loopCounter)) {
            LOOP_DETECTOR.delete(loopCounter);
            return true;
        }
        return false;
    };
    ServiceManager.prototype.set = function (target, instance) {
        this.instances.set(target, instance);
        return this;
    };
    ServiceManager.prototype.get = function (target, options) {
        if (isRequestingSelf(target)) {
            return this;
        }
        var loopCounter = arguments[3];
        if (!loopCounter) {
            loopCounter = Symbol("LOOP_COUNTER");
            LOOP_DETECTOR.set(loopCounter, 0);
        }
        else {
            LOOP_DETECTOR.set(loopCounter, LOOP_DETECTOR.get(loopCounter) + 1);
        }
        if (LOOP_DETECTOR.get(loopCounter) > 99) {
            LOOP_DETECTOR.delete(loopCounter);
            throw new Error("Cyclic dependency detected!");
        }
        var instance;
        var pmHas = false;
        if (this.hasPeeringManager()) {
            for (var i = 0; i < this.peeringManagers.length; i++) {
                var PM_HAS = this.peeringManagers[i]
                    .has.call(this.peeringManagers[i], target, true, loopCounter);
                if (PM_HAS) {
                    pmHas = this.peeringManagers[i];
                }
            }
        }
        // Check the peering Manager first
        if (this.peeringManager && this.peeringManager.has(target, true)) {
            instance = this.peeringManager.get(target, options);
        }
        else if (pmHas !== false) {
            instance = pmHas.get(target, options);
        }
        else {
            /**
             * If we have a shared instance return it
             */
            if (this.instances.has(target)) {
                return this.instances.get(target);
            }
            if (this.factories.has(target)) {
                var factory = this.factories.get(target);
                var FACTORY_INSTANCE = this.get(factory);
                instance = FACTORY_INSTANCE.createService(this, options, target);
            }
            else if (this.di.canInstantiate(target, loopCounter)) {
                instance = this.di.instantiate(target, loopCounter);
            }
        }
        if (!instance) {
            LOOP_DETECTOR.delete(loopCounter);
            throw new Error("Unable to retrieve an instance of " +
                ("" + (target.prototype ? String(target) : target.constructor)));
        }
        if (this.shared.has(target) && this.shared.get(target)) {
            this.instances.set(target, instance);
        }
        LOOP_DETECTOR.delete(loopCounter);
        return instance;
    };
    ServiceManager.prototype.setPeeringManager = function (manager) {
        /**
         * Check the developer is not attempting to create a singularity
         */
        if (manager === this) {
            throw new Error("Attempt to introduce cyclic peering manager, unable to set self as" +
                " peering manager");
        }
        /**
         * Check that the developer is not trying to create a wormhole
         */
        var candidate = manager;
        while (!!candidate.peeringManager) {
            if (candidate === this) {
                throw new Error("Attempt to introduce cyclic peering manager, a parent manager " +
                    "introduces a cyclic reference");
            }
            candidate = candidate.peeringManager;
        }
        // All good
        this._peeringManager = manager;
        return this;
    };
    /**
     * Flag an instance to be shared
     *
     * @param {Function} target The Object to be shared when instantiated
     * @param {boolean} shared The boolean flag indicating the share status of an instance
     * @returns {Manager}
     */
    ServiceManager.prototype.setShared = function (target, shared) {
        if (shared === void 0) { shared = true; }
        this.shared.set(target, shared);
        return this;
    };
    /**
     * Registers a factory for an instance
     * @param {FunctionConstructor} target The target instance prototype
     * @param {IServiceFactory} factory The factory function
     * @returns {Manager}
     */
    ServiceManager.prototype.registerFactory = function (target, factory) {
        this.factories.set(target, factory);
        return this;
    };
    /**
     * Check if there are any peering service locators registered
     * @returns {boolean}
     */
    ServiceManager.prototype.hasPeeringManager = function (peeringManager) {
        if (peeringManager) {
            return !(this.peeringManagers.indexOf(peeringManager) < 0);
        }
        return this.peeringManagers.length > 0;
    };
    /**
     * Add a peering service manager to the instance
     * @param peeringManager
     * @returns {ServiceManager}
     */
    ServiceManager.prototype.addPeeringManager = function (peeringManager) {
        if (this.hasPeeringManager(peeringManager)) {
            throw new Error("Attempt to add duplicate peering service manager");
        }
        this.peeringManagers.push(peeringManager);
        return this;
    };
    /**
     * Remove a peering manager from the instance
     * @param peeringManager
     * @returns {ServiceManager}
     */
    ServiceManager.prototype.removePeeringServiceManager = function (peeringManager) {
        if (this.hasPeeringManager(peeringManager)) {
            this.peeringManagers.splice(this.peeringManagers.indexOf(peeringManager), 1);
        }
        return this;
    };
    return ServiceManager;
}());
exports.ServiceManager = ServiceManager;
//# sourceMappingURL=manager.js.map