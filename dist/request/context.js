"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var timer_1 = require("./timer/timer");
var util_1 = require("util");
var Context = (function () {
    function Context(message) {
        this.message = message;
        this.timer = new timer_1.Timer();
    }
    Context.prototype.getBodyParam = function (param) {
        return this.eval(JSON.parse(this.message.content.toString("utf8")), param);
    };
    Context.prototype.getHeaderParam = function (param) {
        return this.eval(this.message.properties.headers, param);
    };
    Context.prototype.getPropertyParam = function (param) {
        return this.eval(this.message.properties, param);
    };
    Context.prototype.getTimer = function () {
        return this.timer;
    };
    Context.prototype.eval = function (object, expression) {
        var clone = object;
        var pointers = expression.split(/\./g);
        while ((clone = clone[pointers.shift()]) && pointers.length)
            ;
        var VALUE = util_1.isNullOrUndefined(clone) ? null : clone;
        return JSON.parse(JSON.stringify(VALUE));
    };
    return Context;
}());
exports.Context = Context;
//# sourceMappingURL=context.js.map