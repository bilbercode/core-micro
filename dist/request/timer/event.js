"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Event = (function () {
    function Event(name, elapsed) {
        this.name = name;
        this.time = new Date();
        this.elapsed = elapsed;
    }
    return Event;
}());
exports.Event = Event;
//# sourceMappingURL=event.js.map