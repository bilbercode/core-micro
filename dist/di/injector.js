"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = require("../metadata/constants");
require("source-map-support/register");
var Injector = (function () {
    function Injector(serviceLocator, metadata) {
        this.serviceLocator = serviceLocator;
        this.metadata = metadata;
    }
    Injector.prototype.canInstantiate = function (constructor, context) {
        var _this = this;
        if (this.metadata.has(constants_1.DI_INJECTABLE, constructor, this.serviceLocator.id)) {
            var locatedDeps = (this.metadata.has(constants_1.DI_INJECTABLE, constructor, this.serviceLocator.id) ?
                this.metadata.get(constants_1.DI_INJECTABLE, constructor, this.serviceLocator.id) : [])
                .map(function (dep) { return _this.serviceLocator.has.call(_this.serviceLocator, dep, false, context); })
                .filter(function (d) { return !!d; });
            if (locatedDeps.length >= constructor.length) {
                return true;
            }
        }
        return false;
    };
    Injector.prototype.instantiate = function (constructor, context) {
        var _this = this;
        var deps = this.metadata.has(constants_1.DI_INJECTABLE, constructor, this.serviceLocator.id) ?
            this.metadata.get(constants_1.DI_INJECTABLE, constructor, this.serviceLocator.id) : [];
        var ARGS = deps
            .map((function (dep) { return _this.serviceLocator.get.call(_this.serviceLocator, dep, null, context); }));
        return new (constructor.bind.apply(constructor, [void 0].concat(ARGS)))();
    };
    return Injector;
}());
exports.Injector = Injector;
//# sourceMappingURL=injector.js.map