"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var events_1 = require("events");
var isPromise_1 = require("rxjs/util/isPromise");
var Controller = (function (_super) {
    __extends(Controller, _super);
    function Controller(serviceLocator, queue, methods) {
        var _this = _super.call(this) || this;
        _this.serviceLocator = serviceLocator;
        _this.queue = _this.serviceLocator.get(queue);
        _this.methods = methods;
        return _this;
    }
    Controller.prototype.init = function (channel) {
        var _this = this;
        var QUEUE_BINDINGS = this.methods.map(function (METHOD) {
            if (METHOD.binding.exchange) {
                return channel.bindQueue(_this.queue.name, METHOD.binding.exchange.name, METHOD.binding.routingKey, METHOD.binding.args);
            }
        });
        return Promise.all(QUEUE_BINDINGS)
            .then(function () {
            return channel.consume(_this.queue.name, function (MSG) {
                var ROUTE = MSG.fields.routingKey;
                var MATCHES = [];
                _this.methods.forEach(function (METHOD) {
                    var ROUTE_PARTS = METHOD.route.split(".");
                    var weight = ROUTE_PARTS.length;
                    var regStr = ROUTE_PARTS.map(function (P) {
                        if (P === "#") {
                            weight--;
                            return ".*";
                        }
                        if (P === "*") {
                            weight--;
                            return "\\w+";
                        }
                        return P;
                    }).join("\\.");
                    var REGEXP = new RegExp("^" + regStr + "$");
                    if (!!ROUTE.match(REGEXP)) {
                        MATCHES.push({
                            method: METHOD,
                            weight: weight
                        });
                    }
                });
                if (MATCHES.length > 0) {
                    var MATCH = MATCHES
                        .sort(function (A, B) {
                        if (A.weight === B.weight)
                            return 0;
                        return A.weight > B.weight ? -1 : 1;
                    })[0];
                    var OUTCOME = void 0;
                    try {
                        OUTCOME = MATCH.method.dispatch.call(MATCH.method, MSG);
                    }
                    catch (e) {
                        return channel.reject(MSG, false);
                    }
                    if (isPromise_1.isPromise(OUTCOME)) {
                        OUTCOME.then(function (RESPONSE) {
                            channel.ack(MSG);
                            if (MSG.properties.replyTo) {
                                channel
                                    .sendToQueue(MSG.properties.replyTo, new Buffer(JSON.stringify(RESPONSE)), {
                                    contentType: "application/json",
                                    contentEncoding: "utf8",
                                    type: "response.success",
                                    messageId: MSG.properties.messageId || null
                                });
                            }
                        })
                            .catch(function (E) {
                            channel.sendToQueue(MSG.properties.replyTo, new Buffer(JSON.stringify(E)), {
                                contentType: "application/json",
                                contentEncoding: "utf8",
                                type: "response.error",
                                messageId: MSG.properties.messageId || null
                            });
                        });
                    }
                    else {
                        channel.ack(MSG);
                        if (MSG.properties.replyTo) {
                            channel.sendToQueue(MSG.properties.replyTo, new Buffer(JSON.stringify(OUTCOME)), {
                                contentType: "application/json",
                                contentEncoding: "utf8",
                                type: "response.success",
                                messageId: MSG.properties.messageId || null
                            });
                        }
                    }
                }
            });
        });
    };
    return Controller;
}(events_1.EventEmitter));
exports.Controller = Controller;
//# sourceMappingURL=controller.js.map