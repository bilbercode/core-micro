"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var context_1 = require("../request/context");
var context_2 = require("../request/argument/context");
var form_1 = require("../request/argument/form");
var neutrino_meta_1 = require("neutrino-meta");
var constants_1 = require("../metadata/constants");
function isPromise(value) {
    return !!(value instanceof Promise);
}
function isRequestContext(value) {
    return !!(value instanceof context_2.Context);
}
function isRequestForm(value) {
    return !!(value instanceof form_1.Form);
}
var Method = (function () {
    function Method(serviceLocator, controller, method) {
        this.serviceLocator = serviceLocator;
        this.controller = controller;
        this.method = method;
        this.methodParams =
            neutrino_meta_1.Metadata.has(constants_1.ARGUMENT, this.controller, this.method) ?
                neutrino_meta_1.Metadata.get(constants_1.ARGUMENT, this.controller, this.method) : [];
        this.binding = neutrino_meta_1.Metadata.get(constants_1.CONTROLLER_ROUTE, this.controller, this.method);
        if (this.binding.exchange) {
            this.binding.exchange = this.serviceLocator.get(this.binding.exchange);
        }
        this.route = this.binding.routingKey;
    }
    Method.prototype.dispatch = function (message) {
        var _this = this;
        var CONTEXT = this.serviceLocator.get(context_1.Context, message);
        return this.resolveMethodArguments(CONTEXT)
            .then(function (PARAMS) {
            var CONTROLLER = _this.serviceLocator.get(_this.controller);
            return CONTROLLER[_this.method].apply(CONTROLLER, PARAMS);
        });
    };
    Method.prototype.resolveMethodArguments = function (context) {
        var _this = this;
        return Promise.resolve(this.methodParams)
            .then(function (PARAMS) {
            var PARAMS_ASYNC = PARAMS.map(function (PARAM) {
                return Promise.resolve(PARAM)
                    .then(function (P) {
                    if (isRequestForm(P)) {
                        var MODEL_PROTOTYPE = P.model;
                        var FORM_1 = _this.serviceLocator
                            .get(MODEL_PROTOTYPE);
                        return FORM_1.isValid(context)
                            .then(function (OUTCOME) {
                            if (OUTCOME === true) {
                                return FORM_1.getObject();
                            }
                            return Promise.reject(OUTCOME);
                        });
                    }
                    if (isRequestContext(P)) {
                        switch (P.name) {
                            case "context":
                                return context;
                            case "body":
                                return context.getBodyParam(P.expression);
                            case "timer":
                                return context.getTimer();
                            case "header":
                                return context.getHeaderParam(P.expression);
                            case "property":
                                return context.getPropertyParam(P.expression);
                            default:
                                // WTF is this? cant do anything with that
                                return null;
                        }
                    }
                    var CONTROLLER_NAME = _this.controller.
                        prototype.constructor.name;
                    throw new Error("Unknown request argument in " +
                        ("\"" + CONTROLLER_NAME + "::" + _this.method + "\" - ") + P);
                });
            });
            return Promise.all(PARAMS_ASYNC);
        });
    };
    return Method;
}());
exports.Method = Method;
//# sourceMappingURL=method.js.map