"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = require("../metadata/constants");
var util_1 = require("util");
var neutrino_meta_1 = require("neutrino-meta");
function isInput(value) {
    if (util_1.isNullOrUndefined(value))
        return false;
    return util_1.isFunction(value);
}
;
function Input(options) {
    return function (target, propertyKey, descriptor) {
        if (neutrino_meta_1.Metadata.has(constants_1.INPUT, target, propertyKey)) {
            throw new Error("@Input() decorator can only be declared once per property");
        }
        neutrino_meta_1.Metadata.set(constants_1.INPUT, options, target, propertyKey);
        var FORM_INPUTS = neutrino_meta_1.Metadata.has(constants_1.FORM_INPUT, target) ?
            neutrino_meta_1.Metadata.get(constants_1.FORM_INPUT, target) : [];
        FORM_INPUTS.push(propertyKey);
        neutrino_meta_1.Metadata.set(constants_1.FORM_INPUT, FORM_INPUTS, target);
        return descriptor;
    };
}
exports.Input = Input;
//# sourceMappingURL=input.js.map