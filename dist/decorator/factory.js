"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = require("../metadata/constants");
var neutrino_meta_1 = require("neutrino-meta");
function Factory(target, options) {
    return function (factory) {
        if (("createService" in factory.prototype) === false) {
            throw new Error("@Factory decorator can only be declared on a class that implements a" +
                " createService method!");
        }
        neutrino_meta_1.Metadata.set(constants_1.FACTORY, target, factory);
        if (options) {
            neutrino_meta_1.Metadata.set(constants_1.FACTORY_OPTIONS, options, factory);
        }
    };
}
exports.Factory = Factory;
//# sourceMappingURL=factory.js.map