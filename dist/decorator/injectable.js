"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = require("../metadata/constants");
var neutrino_meta_1 = require("neutrino-meta");
function Injectable(options) {
    return function (target) {
        if (neutrino_meta_1.Metadata.has(constants_1.INJECTABLE, target)) {
            throw new Error("@Injectable can only be declared once per class");
        }
        if (!options) {
            options = {
                shared: false
            };
        }
        neutrino_meta_1.Metadata.set(constants_1.INJECTABLE, options, target);
    };
}
exports.Injectable = Injectable;
//# sourceMappingURL=injectable.js.map