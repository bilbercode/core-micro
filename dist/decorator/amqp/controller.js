"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var neutrino_meta_1 = require("neutrino-meta");
var constants_1 = require("../../metadata/constants");
function Controller(options) {
    return function (target) {
        neutrino_meta_1.Metadata.set(constants_1.CONTROLLER, options, target);
    };
}
exports.Controller = Controller;
//# sourceMappingURL=controller.js.map