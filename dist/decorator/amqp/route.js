"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var neutrino_meta_1 = require("neutrino-meta");
var constants_1 = require("../../metadata/constants");
function Route(options) {
    return function (target, method, descriptor) {
        neutrino_meta_1.Metadata.set(constants_1.CONTROLLER_ROUTE, options, target, method);
        return descriptor;
    };
}
exports.Route = Route;
//# sourceMappingURL=route.js.map