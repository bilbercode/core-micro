"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = require("../../metadata/constants");
var context_1 = require("../../request/argument/context");
var neutrino_meta_1 = require("neutrino-meta");
function requestContext(context, expression) {
    if (expression === void 0) { expression = null; }
    return function (target, method, i) {
        var FUNCTION_ARGUMENTS = neutrino_meta_1.Metadata.has(constants_1.ARGUMENT, target, method) ?
            neutrino_meta_1.Metadata.get(constants_1.ARGUMENT, target, method) : [];
        FUNCTION_ARGUMENTS[i] = new context_1.Context(context, expression);
        neutrino_meta_1.Metadata.set(constants_1.ARGUMENT, FUNCTION_ARGUMENTS, target, method);
    };
}
exports.requestContext = requestContext;
//# sourceMappingURL=request-context.js.map