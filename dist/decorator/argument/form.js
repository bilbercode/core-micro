"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = require("../../metadata/constants");
var form_1 = require("../../request/argument/form");
var neutrino_meta_1 = require("neutrino-meta");
function Form(form) {
    return function (target, method, i) {
        var FUNCTION_ARGUMENTS = neutrino_meta_1.Metadata.has(constants_1.ARGUMENT, target, method) ?
            neutrino_meta_1.Metadata.get(constants_1.ARGUMENT, target, method) : [];
        FUNCTION_ARGUMENTS[i] = new form_1.Form(form);
        neutrino_meta_1.Metadata.set(constants_1.ARGUMENT, FUNCTION_ARGUMENTS, target, method);
    };
}
exports.Form = Form;
//# sourceMappingURL=form.js.map