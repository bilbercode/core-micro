"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var injectable_1 = require("./injectable");
var factory_1 = require("./factory");
var form_1 = require("./form");
var ArgumentDecorators = require("./argument");
var input_1 = require("./input");
var queue_1 = require("./queue");
var exchange_1 = require("./exchange");
var AmqpModule = require("./amqp");
/* tslint:disable */
var Decorator;
(function (Decorator) {
    Decorator.Controller = AmqpModule.Controller;
    Decorator.Route = AmqpModule.Route;
    Decorator.Injectable = injectable_1.Injectable;
    Decorator.Factory = factory_1.Factory;
    Decorator.Form = form_1.Form;
    Decorator.Argument = ArgumentDecorators;
    Decorator.Input = input_1.Input;
    Decorator.Queue = queue_1.Queue;
    Decorator.Exchange = exchange_1.Exchange;
})(Decorator = exports.Decorator || (exports.Decorator = {}));
/* tslint:enable */
//# sourceMappingURL=index.js.map