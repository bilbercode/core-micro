"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = require("../metadata/constants");
var neutrino_meta_1 = require("neutrino-meta");
function Exchange() {
    return function (target) {
        if (neutrino_meta_1.Metadata.has(constants_1.INJECTABLE, target)) {
            throw new Error("@Injectable can only be declared once per class");
        }
        neutrino_meta_1.Metadata.set(constants_1.INJECTABLE, { shared: true }, target);
        neutrino_meta_1.Metadata.set(constants_1.AMQP_EXCHANGE, true, target);
    };
}
exports.Exchange = Exchange;
//# sourceMappingURL=exchange.js.map