"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = require("../metadata/constants");
var neutrino_meta_1 = require("neutrino-meta");
function Form() {
    return function (target) {
        if (neutrino_meta_1.Metadata.has(constants_1.FORM, target)) {
            throw new Error("@Form decorator can only be declared once per class");
        }
        neutrino_meta_1.Metadata.set(constants_1.FORM, true, target);
    };
}
exports.Form = Form;
//# sourceMappingURL=form.js.map