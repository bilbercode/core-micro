"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var manager_1 = require("../service/manager");
var application_1 = require("./factory/application");
var constants_1 = require("../metadata/constants");
var neutrino_meta_1 = require("neutrino-meta");
function microServiceBootstrap(application) {
    var APPLICATION_SERVICE_MANAGER = new manager_1.ServiceManager(neutrino_meta_1.Metadata);
    neutrino_meta_1.Metadata.set(constants_1.DI_INJECTABLE, [], application_1.ApplicationFactory, APPLICATION_SERVICE_MANAGER.id);
    APPLICATION_SERVICE_MANAGER.registerFactory(application, application_1.ApplicationFactory);
    return APPLICATION_SERVICE_MANAGER.get(application);
}
exports.microServiceBootstrap = microServiceBootstrap;
//# sourceMappingURL=bootstrap.js.map