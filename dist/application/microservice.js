"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var events_1 = require("events");
var util_1 = require("util");
var neutrino_meta_1 = require("neutrino-meta");
var constants_1 = require("../metadata/constants");
var manager_1 = require("../service/manager");
var amqp_1 = require("../amqp/factory/amqp");
var module_1 = require("./factory/module");
function MicroService(options) {
    return function (target) {
        function ModuleClassDecorator(constructor) {
            var NEW_CLASS = (function (_super) {
                __extends(Module, _super);
                function Module() {
                    var args = [];
                    for (var _i = 0; _i < arguments.length; _i++) {
                        args[_i] = arguments[_i];
                    }
                    var _this = _super.apply(this, args.slice(1)) || this;
                    _this.serviceLocator = args[0];
                    _this.on("error", function (e, context) {
                        if ("$onError" in _this) {
                            _this.$onError(e, context);
                        }
                    });
                    return _this;
                }
                Module.prototype.start = function (config) {
                    var _this = this;
                    return Promise.resolve()
                        .then(function () { return "$onInit" in _this ? _this
                        .$onInit.call(_this) : null; })
                        .then(function () { return _this.createModules(); })
                        .then(function () { return _this.initModuleProviders(); })
                        .then(function () { return _this.initModuleControllers(); })
                        .then(function () { return _this.initChannel(config); })
                        .then(function () { return _this.initExchanges(); })
                        .then(function () { return _this.initQueues(); })
                        .then(function () { return _this.startControllers(); })
                        .then(function () { return "$onReady" in _this ? _this
                        .$onReady.call(_this) : null; })
                        .catch(function (e) { return _this.emit("error", e); });
                };
                Module.prototype.createModules = function () {
                    var _this = this;
                    this.modules = options.modules.map(function (MODULE) {
                        _this.serviceLocator.registerFactory(MODULE, module_1.ModuleFactory);
                        return _this.serviceLocator.get(MODULE);
                    });
                    return Promise.resolve();
                };
                Module.prototype.initModuleProviders = function () {
                    return Promise.all(this.modules.map(function (MODULE) { return MODULE.initProviders(); }));
                };
                Module.prototype.initModuleControllers = function () {
                    return Promise.all(this.modules.map(function (MODULE) { return MODULE.initControllers(); }));
                };
                Module.prototype.initChannel = function (config) {
                    var _this = this;
                    var PROMISE = this.serviceLocator.get(amqp_1.RABBIT_MQ_CLIENT, config);
                    return PROMISE.then(function (CONNECTION) {
                        _this.connection = CONNECTION;
                        return CONNECTION.createChannel()
                            .then(function (CHANNEL) {
                            _this.channel = CHANNEL;
                            return _this.channel;
                        });
                    });
                };
                Module.prototype.initExchanges = function () {
                    var _this = this;
                    var EXCHANGES_INIT = [];
                    this.modules.forEach(function (MODULE) {
                        MODULE.exchanges.map(function (EXCHANGE) {
                            EXCHANGES_INIT.push(_this.channel
                                .assertExchange(EXCHANGE.name, EXCHANGE.type, EXCHANGE.options)
                                .then(function () {
                                return EXCHANGE;
                            }));
                        });
                    });
                    return Promise.all(EXCHANGES_INIT)
                        .then(function (EXCHANGES) {
                        return Promise.all(EXCHANGES.map(function (EXCHANGE) {
                            if (EXCHANGE.bindings && EXCHANGE.bindings.length > 0) {
                                return Promise.all(EXCHANGE.bindings.map(function (BINDING) {
                                    _this.channel
                                        .bindExchange(BINDING.destination.name, EXCHANGE.name, BINDING.routingKey, BINDING.args);
                                }));
                            }
                        }));
                    });
                };
                Module.prototype.initQueues = function () {
                    var _this = this;
                    return Promise.all(this.modules.map(function (MODULE) {
                        return Promise.all(MODULE.queues.map(function (QUEUE) {
                            return _this.channel.assertQueue(QUEUE.name, QUEUE.options);
                        }));
                    }));
                };
                Module.prototype.startControllers = function () {
                    var _this = this;
                    return Promise.all(this.modules.map(function (MODULE) {
                        return Promise.all(MODULE.controllers.map(function (CONTROLLER) {
                            return CONTROLLER.init(_this.channel);
                        }));
                    }));
                };
                return Module;
            }(constructor));
            var PARAMS = neutrino_meta_1.Metadata.has(constants_1.DESIGN_PARAM_TYPES, target) ?
                neutrino_meta_1.Metadata.get(constants_1.DESIGN_PARAM_TYPES, target) : [];
            PARAMS.unshift(manager_1.ServiceManager);
            neutrino_meta_1.Metadata.set(constants_1.DI_INJECTABLE, PARAMS, NEW_CLASS);
            return NEW_CLASS;
        }
        ;
        function decorateEventEmitter(constructor) {
            var ORIGIN = constructor.constructor;
            constructor.constructor = function () {
                var args = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    args[_i] = arguments[_i];
                }
                ORIGIN.apply(this, args);
                events_1.EventEmitter.call(this);
            };
            util_1.inherits(constructor, events_1.EventEmitter);
            return constructor;
        }
        return ModuleClassDecorator(decorateEventEmitter(target));
    };
}
exports.MicroService = MicroService;
//# sourceMappingURL=microservice.js.map