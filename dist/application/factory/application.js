"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var manager_1 = require("../../service/manager");
var constants_1 = require("../../metadata/constants");
var module_1 = require("./service-locator/module");
var module_2 = require("./module");
var neutrino_meta_1 = require("neutrino-meta");
var amqp_1 = require("../../amqp/factory/amqp");
var ApplicationFactory = (function () {
    function ApplicationFactory() {
    }
    ApplicationFactory.prototype.createService = function (serviceLocator, options, target) {
        var APPLICATION_SERVICE_LOCATOR = new manager_1.ServiceManager(neutrino_meta_1.Metadata);
        neutrino_meta_1.Metadata.set(constants_1.DI_INJECTABLE, [], module_1.ModuleServiceLocatorFactory, APPLICATION_SERVICE_LOCATOR.id);
        neutrino_meta_1.Metadata.set(constants_1.DI_INJECTABLE, [], module_2.ModuleFactory, APPLICATION_SERVICE_LOCATOR.id);
        APPLICATION_SERVICE_LOCATOR.registerFactory(constants_1.MODULE_SERVICE_LOCATOR, module_1.ModuleServiceLocatorFactory);
        APPLICATION_SERVICE_LOCATOR.registerFactory(amqp_1.RABBIT_MQ_CLIENT, amqp_1.AmqpClient);
        this.registerInjectable(amqp_1.AmqpClient, APPLICATION_SERVICE_LOCATOR.id);
        var PARAMS = neutrino_meta_1.Metadata.get(constants_1.DI_INJECTABLE, target);
        var INJECTIONS = PARAMS.map(function (PARAM, i) {
            switch (i) {
                case 0:
                    return APPLICATION_SERVICE_LOCATOR;
                default:
                    return APPLICATION_SERVICE_LOCATOR.get(PARAM);
            }
        });
        return new (target.bind.apply(target, [void 0].concat(INJECTIONS)))();
    };
    ApplicationFactory.prototype.registerInjectable = function (injectable, slID) {
        var PARAMS = neutrino_meta_1.Metadata.has(constants_1.DESIGN_PARAM_TYPES, injectable) ?
            neutrino_meta_1.Metadata.get(constants_1.DESIGN_PARAM_TYPES, injectable) : [];
        neutrino_meta_1.Metadata.set(constants_1.DI_INJECTABLE, PARAMS, injectable, slID);
    };
    return ApplicationFactory;
}());
exports.ApplicationFactory = ApplicationFactory;
//# sourceMappingURL=application.js.map