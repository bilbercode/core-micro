"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var neutrino_meta_1 = require("neutrino-meta");
var constants_1 = require("../../metadata/constants");
var method_1 = require("../../dispatcher/method");
;
var ControllerFactory = (function () {
    function ControllerFactory() {
    }
    ControllerFactory.prototype.createService = function (serviceLocator, options, constructor) {
        var METHODS = []; // TODO methods from options.dispachable
        Object.getOwnPropertyNames(options.dispatchable.prototype)
            .forEach(function (METHOD) {
            if (neutrino_meta_1.Metadata.has(constants_1.CONTROLLER_ROUTE, options.dispatchable, METHOD)) {
                METHODS.push(serviceLocator.get(method_1.Method, {
                    dispatchable: options.dispatchable,
                    method: METHOD
                }));
            }
        });
        return new constructor(serviceLocator, options.queue, METHODS);
    };
    return ControllerFactory;
}());
exports.ControllerFactory = ControllerFactory;
//# sourceMappingURL=controller.js.map