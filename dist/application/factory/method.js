"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var method_1 = require("../../dispatcher/method");
var MethodFactory = (function () {
    function MethodFactory() {
    }
    MethodFactory.prototype.createService = function (serviceLocator, options, constructor) {
        return new method_1.Method(serviceLocator, options.dispatchable, options.method);
    };
    return MethodFactory;
}());
exports.MethodFactory = MethodFactory;
//# sourceMappingURL=method.js.map