"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var manager_1 = require("../../../service/manager");
var whitelist_1 = require("./whitelist");
var whitelisted_manager_1 = require("../../../service/whitelisted-manager");
var constants_1 = require("../../../metadata/constants");
var trim_string_1 = require("../../../form/input/filter/trim-string");
var strip_tags_1 = require("../../../form/input/filter/strip-tags");
var not_empty_1 = require("../../../form/input/validator/not-empty");
var form_1 = require("../../../form/form");
var form_2 = require("../../../form/factory/form");
var input_1 = require("../../../form/input/input");
var input_2 = require("../../../form/factory/input");
var neutrino_meta_1 = require("neutrino-meta");
var controller_1 = require("../../../dispatcher/controller");
var controller_2 = require("../controller");
var method_1 = require("../../../dispatcher/method");
var method_2 = require("../method");
var context_1 = require("../../../request/context");
var context_2 = require("../request/context");
var ModuleServiceLocatorFactory = (function () {
    function ModuleServiceLocatorFactory() {
    }
    ModuleServiceLocatorFactory.prototype.createService = function (serviceLocator, options) {
        var _this = this;
        var SERVICE_LOCATOR = new manager_1.ServiceManager(neutrino_meta_1.Metadata);
        SERVICE_LOCATOR.registerFactory(form_1.Form, form_2.FormFactory);
        SERVICE_LOCATOR.registerFactory(input_1.Input, input_2.InputFactory);
        SERVICE_LOCATOR.registerFactory(controller_1.Controller, controller_2.ControllerFactory);
        SERVICE_LOCATOR.registerFactory(method_1.Method, method_2.MethodFactory);
        SERVICE_LOCATOR.registerFactory(context_1.Context, context_2.ContextFactory);
        SERVICE_LOCATOR.registerFactory(whitelisted_manager_1.WhitelistedServiceManager, whitelist_1.WhitelistServiceLocatorFactory);
        var INJECTABLES = [
            trim_string_1.TrimString,
            strip_tags_1.StripTags,
            not_empty_1.NotEmpty,
            whitelist_1.WhitelistServiceLocatorFactory,
            form_2.FormFactory,
            input_2.InputFactory,
            controller_2.ControllerFactory,
            method_2.MethodFactory,
            context_2.ContextFactory,
        ];
        INJECTABLES.forEach(function (INJECTABLE) {
            _this.registerInjectable(INJECTABLE, SERVICE_LOCATOR.id);
        });
        SERVICE_LOCATOR.set(neutrino_meta_1.Metadata, neutrino_meta_1.Metadata);
        return SERVICE_LOCATOR;
    };
    ModuleServiceLocatorFactory.prototype.registerInjectable = function (injectable, slID) {
        var PARAMS = neutrino_meta_1.Metadata.has(constants_1.DESIGN_PARAM_TYPES, injectable) ?
            neutrino_meta_1.Metadata.get(constants_1.DESIGN_PARAM_TYPES, injectable) : [];
        neutrino_meta_1.Metadata.set(constants_1.DI_INJECTABLE, PARAMS, injectable, slID);
    };
    return ModuleServiceLocatorFactory;
}());
exports.ModuleServiceLocatorFactory = ModuleServiceLocatorFactory;
//# sourceMappingURL=module.js.map