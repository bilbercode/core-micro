"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var context_1 = require("../../../request/context");
var ContextFactory = (function () {
    function ContextFactory() {
    }
    ContextFactory.prototype.createService = function (serviceLocator, message, constructor) {
        return new context_1.Context(message);
    };
    return ContextFactory;
}());
exports.ContextFactory = ContextFactory;
//# sourceMappingURL=context.js.map