"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var injector_1 = require("../../di/injector");
var constants_1 = require("../../metadata/constants");
var neutrino_meta_1 = require("neutrino-meta");
var ModuleFactory = (function () {
    function ModuleFactory() {
    }
    ModuleFactory.prototype.createService = function (serviceLocator, options, target) {
        var SERVICE_MANAGER = serviceLocator.get(constants_1.MODULE_SERVICE_LOCATOR);
        neutrino_meta_1.Metadata.set(constants_1.DI_INJECTABLE, [], neutrino_meta_1.Metadata, SERVICE_MANAGER.id);
        var METADATA = neutrino_meta_1.Metadata;
        var DEPENDENCY_INJECTOR = new injector_1.Injector(SERVICE_MANAGER, METADATA);
        if (target.providers) {
            target.providers.forEach(function (COMPONENT) {
                if (METADATA.has(constants_1.FACTORY, COMPONENT)) {
                    var FACTORY_CONFIG = METADATA.has(constants_1.FACTORY_OPTIONS, COMPONENT) ?
                        METADATA.get(constants_1.FACTORY_OPTIONS, COMPONENT) : { shared: false, exported: false };
                    SERVICE_MANAGER
                        .registerFactory(METADATA.get(constants_1.FACTORY, COMPONENT), COMPONENT);
                    SERVICE_MANAGER.setShared(METADATA.get(constants_1.FACTORY, COMPONENT), FACTORY_CONFIG.shared || false);
                }
            });
        }
        var TARGET = METADATA.get(constants_1.APPLICATION_MODULE, target);
        var INJ_ARGS = METADATA.has(constants_1.DI_INJECTABLE, TARGET) ?
            METADATA.get(constants_1.DI_INJECTABLE, TARGET) : [];
        var INSTANCE_ARGS = INJ_ARGS.map(function (dep, i) {
            switch (i) {
                case 0:
                    return SERVICE_MANAGER;
                case 1:
                    return METADATA;
                default:
                    return SERVICE_MANAGER.get.call(SERVICE_MANAGER, dep, null);
            }
        });
        var INSTANCE = new (target.bind.apply(target, [void 0].concat(INSTANCE_ARGS)))();
        return INSTANCE;
    };
    return ModuleFactory;
}());
exports.ModuleFactory = ModuleFactory;
//# sourceMappingURL=module.js.map