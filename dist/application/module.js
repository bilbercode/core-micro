"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
require("source-map-support/register");
var manager_1 = require("../service/manager");
var constants_1 = require("../metadata/constants");
var whitelisted_manager_1 = require("../service/whitelisted-manager");
var events_1 = require("events");
var Util = require("util");
var form_1 = require("../form/factory/form");
var neutrino_meta_1 = require("neutrino-meta");
var controller_1 = require("../dispatcher/controller");
function Module(options) {
    return function (target) {
        var exportedServiceLocator = new whitelisted_manager_1.WhitelistedServiceManager(neutrino_meta_1.Metadata);
        function ModuleClassDecorator(constructor) {
            var NEW_CLASS = (function (_super) {
                __extends(Module, _super);
                function Module() {
                    var args = [];
                    for (var _i = 0; _i < arguments.length; _i++) {
                        args[_i] = arguments[_i];
                    }
                    var _this = _super.apply(this, args.slice(2)) || this;
                    _this.name = options.name;
                    _this.controllers = [];
                    _this.import = options.import;
                    _this.providers = options.providers;
                    _this.exchanges = [];
                    _this.queues = [];
                    _this.serviceLocator = args[0];
                    _this.meta = args[1];
                    _this.exportedServiceLocator.setWhitelist(_this.export);
                    _this.exportedServiceLocator.setPeeringManager(_this.serviceLocator);
                    return _this;
                }
                Object.defineProperty(Module, "exportedServiceLocator", {
                    get: function () {
                        return exportedServiceLocator;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Module, "export", {
                    get: function () {
                        return options.export;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Module.prototype, "export", {
                    get: function () {
                        return options.export;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Module.prototype, "exportedServiceLocator", {
                    get: function () {
                        return exportedServiceLocator;
                    },
                    set: function (serviceLocator) {
                        exportedServiceLocator = serviceLocator;
                    },
                    enumerable: true,
                    configurable: true
                });
                Module.prototype.initControllers = function () {
                    var _this = this;
                    if (options.controllers) {
                        options.controllers
                            .forEach(function (COMPONENT) {
                            /**
                             * Register controllers with their respective dispatcher
                             */
                            if (_this.meta.has(constants_1.CONTROLLER, COMPONENT)) {
                                var PARAMS_1 = _this.meta.has(constants_1.DESIGN_PARAM_TYPES, COMPONENT) ?
                                    _this.meta.get(constants_1.DESIGN_PARAM_TYPES, COMPONENT) : [];
                                _this.meta.set(constants_1.DI_INJECTABLE, PARAMS_1, COMPONENT, _this.serviceLocator.id);
                                var CONTROLLER_INSTANCE = _this.serviceLocator
                                    .get(controller_1.Controller, {
                                    dispatchable: COMPONENT,
                                    queue: neutrino_meta_1.Metadata.get(constants_1.CONTROLLER, COMPONENT)
                                });
                                _this.serviceLocator.setShared(COMPONENT, true);
                                CONTROLLER_INSTANCE.on("error", function (msg) {
                                    _this.emit("error", msg);
                                });
                                _this.controllers.push(CONTROLLER_INSTANCE);
                            }
                        });
                    }
                    return Promise.resolve(this);
                };
                Module.prototype.initProviders = function () {
                    var _this = this;
                    if (this.import) {
                        this.import.forEach(function (MODULE) {
                            _this.serviceLocator.addPeeringManager(MODULE.exportedServiceLocator);
                        });
                    }
                    if (this.providers) {
                        this.providers
                            .forEach(function (COMPONENT) {
                            if (_this.meta.has(constants_1.FORM, COMPONENT)) {
                                var PARAMS_2 = _this.meta.has(constants_1.DESIGN_PARAM_TYPES, COMPONENT) ?
                                    _this.meta.get(constants_1.DESIGN_PARAM_TYPES, COMPONENT) : [];
                                _this.meta.set(constants_1.DI_INJECTABLE, PARAMS_2, COMPONENT, _this.serviceLocator.id);
                                _this.serviceLocator.registerFactory(COMPONENT, form_1.FormFactory);
                            }
                            if (_this.meta.has(constants_1.FACTORY, COMPONENT)) {
                                var PARAMS_3 = _this.meta.has(constants_1.DESIGN_PARAM_TYPES, COMPONENT) ?
                                    _this.meta.get(constants_1.DESIGN_PARAM_TYPES, COMPONENT) : [];
                                _this.meta.set(constants_1.DI_INJECTABLE, PARAMS_3, COMPONENT, _this.serviceLocator.id);
                                var FACTORY_CONFIG = _this.meta.has(constants_1.FACTORY_OPTIONS, COMPONENT) ?
                                    _this.meta.get(constants_1.FACTORY_OPTIONS, COMPONENT) : {
                                    shared: false,
                                    exported: false
                                };
                                var SERVICE_LOCATOR = _this.serviceLocator;
                                SERVICE_LOCATOR
                                    .registerFactory(_this.meta.get(constants_1.FACTORY, COMPONENT), COMPONENT);
                                SERVICE_LOCATOR.setShared(_this.meta.get(constants_1.FACTORY, COMPONENT), FACTORY_CONFIG.shared || false);
                            }
                            if (_this.meta.has(constants_1.INJECTABLE, COMPONENT)) {
                                var INJ_OPT = _this.meta.get(constants_1.INJECTABLE, COMPONENT);
                                var PARAMS_4 = _this.meta.has(constants_1.DESIGN_PARAM_TYPES, COMPONENT) ?
                                    _this.meta.get(constants_1.DESIGN_PARAM_TYPES, COMPONENT) : [];
                                _this.meta.set(constants_1.DI_INJECTABLE, PARAMS_4, COMPONENT, _this.serviceLocator.id);
                                if (INJ_OPT.shared) {
                                    _this.serviceLocator.setShared(COMPONENT, true);
                                }
                            }
                            if (_this.meta.has(constants_1.AMQP_EXCHANGE, COMPONENT)) {
                                _this.exchanges.push(new COMPONENT());
                            }
                            if (_this.meta.has(constants_1.AMQP_QUEUE, COMPONENT)) {
                                _this.queues.push(new COMPONENT());
                            }
                        });
                    }
                    return Promise.resolve(this);
                };
                return Module;
            }(constructor));
            neutrino_meta_1.Metadata.set(constants_1.INJECTABLE, options, target);
            var PARAMS = neutrino_meta_1.Metadata.has(constants_1.DESIGN_PARAM_TYPES, target) ?
                neutrino_meta_1.Metadata.get(constants_1.DESIGN_PARAM_TYPES, target) : [];
            PARAMS.unshift(neutrino_meta_1.Metadata);
            PARAMS.unshift(manager_1.ServiceManager);
            neutrino_meta_1.Metadata.set(constants_1.DI_INJECTABLE, PARAMS, target);
            neutrino_meta_1.Metadata.set(constants_1.APPLICATION_MODULE, target, NEW_CLASS);
            return NEW_CLASS;
        }
        function decorateEventEmitter(constructor) {
            var ORIGIN = constructor.constructor;
            constructor.constructor = function () {
                var args = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    args[_i] = arguments[_i];
                }
                ORIGIN.apply(this, args);
                events_1.EventEmitter.call(this);
            };
            Util.inherits(constructor, events_1.EventEmitter);
            return constructor;
        }
        return ModuleClassDecorator(decorateEventEmitter(target));
    };
}
exports.Module = Module;
//# sourceMappingURL=module.js.map