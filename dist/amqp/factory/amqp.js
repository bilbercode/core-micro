"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var factory_1 = require("../../decorator/factory");
var amqplib_1 = require("amqplib");
exports.RABBIT_MQ_CLIENT = Symbol("RABBIT_MQ_CLIENT");
var AmqpClient = (function () {
    function AmqpClient() {
    }
    AmqpClient.prototype.createService = function (serviceLocator, options, constructor) {
        var url = "amqp://" + options.username + ":" + options.password;
        url += "@" + options.host + ":" + options.port;
        if (options.virtualhost) {
            url += "/" + options.virtualhost;
        }
        return amqplib_1.connect(url);
    };
    return AmqpClient;
}());
AmqpClient = __decorate([
    factory_1.Factory(exports.RABBIT_MQ_CLIENT, { shared: true })
], AmqpClient);
exports.AmqpClient = AmqpClient;
//# sourceMappingURL=amqp.js.map