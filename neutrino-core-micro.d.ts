import { Metadata } from "neutrino-meta";
export interface IExchange {
  name: string;
  options?: {
    durable?: boolean;
    internal?: boolean;
    autoDelete?: boolean;
    alternateExchange?: string;
    arguments?: any;
  };
  type: string;
  bindings?: Array<IExchangeBinding>;
}

export interface IExchangeBinding {
  routingKey: string;
  destination: IExchange;
  args: { [argument: string]: string };
}

export interface IQueue {
  name: string;
  options?: {
    exclusive?: boolean;
    durable?: boolean;
    autoDelete?: boolean;
    arguments?: any;
    messageTtl?: number;
    expires?: number;
    deadLetterExchange?: string;
    deadLetterRoutingKey?: string;
    maxLength?: number;
    maxPriority?: number;
  };
}


export interface IQueueBinding {
  routingKey: string;
  exchange: IExchange;
  args?: { [argument: string]: string };
}

export interface ILifecycleMicroService {
  $onInit?(): Promise<any>;
  $onReady?(): Promise<any>;
  $onError?(err: Error|string): Promise<any>;
}

export interface MicroServiceConfiguration {
  host: string;
  port: number;
  username: string;
  password: string;
  virtualhost?: string;
}

export interface IMicroService {
  start(config: MicroServiceConfiguration): Promise<any>;
}

export function microServiceBootstrap<T>(application: T): IMicroService;


interface IModuleOptions {
  name: string;
  controllers?: Array<Function>;
  providers?: Array<Function>;
  export?: Array<Function|symbol>;
  import?: Array<any>;
}

export function Module(options: IModuleOptions): Function;

export interface IApplicationOptions {
  name: string;
  modules: Array<Function>;
}

export function MicroService(options: IApplicationOptions): Function;



export declare class ServiceManager {

  peeringManager: ServiceManager;

  constructor(metadata: Metadata);

  has(target: any, ignoreDI?: boolean): boolean;

  set(target: any, instance: any): ServiceManager;

  get<T>(target: any, options?: any): T ;

  setPeeringManager(manager: ServiceManager): ServiceManager;

  /**
   * Flag an instance to be shared
   *
   * @param {Function} target The Object to be shared when instantiated
   * @param {boolean} shared The boolean flag indicating the share status of an instance
   * @returns {Manager}
   */
  setShared(target: Function, shared?: boolean): ServiceManager;

  /**
   * Registers a factory for an instance
   * @param {FunctionConstructor} target The target instance prototype
   * @param {IServiceFactory} factory The factory function
   * @returns {Manager}
   */
  registerFactory(target: any, factory: FunctionConstructor|Function): ServiceManager;
}

export interface IFactory {
  createService<T>(serviceLocator: ServiceManager, options?: any): T;
}


export namespace Decorator {

  export interface IControllerOptions {
    queue: IQueue;
  }

  export function Controller(options: IQueue): Function;

  export function Route(options: IQueueBinding): Function;

  export function Exchange(): Function;

  export function Queue(): Function;

  export namespace Argument {

    export function Body(expression: string): Function;

    export function Header(expression: string): Function;

    export function AddOn(expression: string): Function;

    export function Context(): Function;

    export function Exchange(): Function;

    export function Queue(): Function;

    export function Form(target: Function): Function;

    export function Timer(): Function;

    export function Property(expression: string): Function;
  }

  interface IFactoryOptions {
    exported?: boolean;
    shared?: boolean;
  }

  export function Factory(taget: any, options?: IFactoryOptions): Function;

  export function Injectable(options?: IFactoryOptions): Function;

  export enum EInputSource {
    bodyParam,
    headerParam,
    propertyParam,
  }

  interface IInputOptions {
    source: EInputSource;
    options?: any;
    inputName?: string|number;
    validators?: Array<Form.Input.Validator.IValidator|Function>;
    filters?: Array<Form.Input.Filter.IFilter|Function>;
  }

  export function Input(options: IInputOptions): Function;

  export function Form(): Function;
}


export namespace Form {
  export namespace Input {

    export enum EInputSource {
      bodyParam,
      headerParam,
      propertyParam,
    }

    export interface IInput {
      property: string;
      inputName: string;
      isValid(value: any, context?: any): Promise<Response>;
    }

    export class Response {
      public readonly input: IInput;
      public readonly raw: string;
      public readonly clean: any;
      public readonly messages: any;
      public readonly isValid: boolean;
    }

    class Input implements IInput {
      public readonly property;
      public readonly inputName;
      isValid(value: any, context?: any): Promise<Response>;
    }

    export namespace Validator {
      export interface IValidator {
        name: string;
        validate(value: string|any, context: any): Promise<boolean|string>|boolean|string;
      }

      export class NotEmpty implements IValidator {
        name: string;
        validate(value: string, context: Array<any>): Promise<boolean|string>|boolean|string;
      }
    }

    export namespace Filter {
      export interface IFilter {
        filter(value: string|any, context?: any): Promise<any>|any;
      }

      export class StripTags implements IFilter {
        filter(value: string): Promise<any>|any;
      }

      export class TrimString implements IFilter {
        filter(value: string): Promise<any>|any;
      }
    }
  }
}

export declare class Timer {
  mark(event: string);
}
