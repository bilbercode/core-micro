

import { IFactory } from "../../service/factory";
import { ServiceManager } from "../../service/manager";
import { Factory } from "../../decorator/factory";
import { connect, Connection } from "amqplib";
import Bluebird = require("bluebird");


export const RABBIT_MQ_CLIENT = Symbol("RABBIT_MQ_CLIENT");

interface IMicroServiceConfiguration {
  host: string;
  port: number;
  username: string;
  password: string;
  virtualhost: string;
}

@Factory(RABBIT_MQ_CLIENT, { shared: true })
export class AmqpClient implements IFactory {


  createService<T>(serviceLocator: ServiceManager, options: IMicroServiceConfiguration, constructor?: Function): Bluebird<Connection> {


    let url = `amqp://${options.username}:${options.password}`;
    url += `@${options.host}:${options.port}`;
    if (options.virtualhost) {
      url += `/${options.virtualhost}`;
    }

    return connect(url);
  }
}
