

export interface IExchange {
  name: string;
  options?: {
    durable?: boolean;
    internal?: boolean;
    autoDelete?: boolean;
    alternateExchange?: string;
    arguments?: any;
  };
  type: string;
  bindings: Array<IExchangeBinding>;
}

export interface IExchangeBinding {
  routingKey: string;
  destination: IExchange;
  args: { [argument: string]: string };
}
