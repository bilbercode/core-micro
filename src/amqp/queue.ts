

import { IExchange } from "./exchange";
export interface IQueue {
  name: string;
  options?: {
    exclusive?: boolean;
    durable?: boolean;
    autoDelete?: boolean;
    arguments?: any;
    messageTtl?: number;
    expires?: number;
    deadLetterExchange?: string;
    deadLetterRoutingKey?: string;
    maxLength?: number;
    maxPriority?: number;
  };
  bindings?: Array<IQueueBinding>;
}


export interface IQueueBinding {
  routingKey: string;
  exchange: IExchange;
  args?: { [argument: string]: string };
}
