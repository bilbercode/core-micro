import { IValidator } from "./validator";
import { isNullOrUndefined } from "util";
import { Injectable } from "../../../decorator/injectable";

@Injectable()
export class NotEmpty implements IValidator {


  public readonly name = "NotEmpty";

  validate(value: string): boolean|string {
    if (isNullOrUndefined(value) || String(value) === "") {
      return "Required";
    }
    return true;
  }
}
