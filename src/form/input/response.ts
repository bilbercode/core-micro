import { IInput } from "./input";

export class Response {

  constructor(public readonly input: IInput,
              public readonly raw: string,
              public readonly clean: any,
              public readonly messages: any,
              public readonly isValid: boolean) { }
}
