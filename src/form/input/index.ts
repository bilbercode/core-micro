import * as Filters from "./filter";
import * as Validators from "./validator";
export { EInputSource, IInput } from "./input";
export * from "./response";


export namespace Filter {
  export interface IFilter extends Filters.IFilter { }
  export const StripTags = Filters.StripTags;
  export const TrimString = Filters.TrimString;
}

export namespace Validator {
  export interface IValidator extends Validators.IValidator { }
  export const NotEmpty = Validators.NotEmpty;
}


