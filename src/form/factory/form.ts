

import { IFactory } from "../../service/factory";
import { Form } from "./../form";
import { ServiceManager } from "../../service/manager";
import { INPUT, FORM_INPUT, DI_INJECTABLE } from "../../metadata/constants";
import { IInput, Input } from "./../input/input";
import { IInputOptions } from "../../decorator/input";
import { Metadata } from "neutrino-meta";


interface FormFactoryOptions {
  model: Function;
}

export class FormFactory implements IFactory {


  createService<T>(serviceLocator: ServiceManager, options?: Function, model?: any): Form {
    if (!model) {
      return null;
    }

    const MODEL_DEPS = (Metadata.has(DI_INJECTABLE, model) ?
      Metadata.get<Array<any>>(DI_INJECTABLE, model) : [])
      .map(DEP => {
        return serviceLocator.get(DEP);
      });

    const BOUND_INSTANCE = new model(...MODEL_DEPS);

    const FORM = new Form(BOUND_INSTANCE);

    const META: Metadata = serviceLocator.get<Metadata>(Metadata);

    const FORM_INPUTS = META.has(FORM_INPUT, model) ?
      META.get<Array<string>>(FORM_INPUT, model) : [];

    FORM_INPUTS
      .forEach(PROP => {
        if (META.has(INPUT, model, PROP)) {
          const OPTIONS = META.get<IInputOptions>(INPUT, model, PROP);

          const FORM_INPUT: IInput = serviceLocator.get<IInput>(Input, {
            source: OPTIONS.source,
            property: PROP,
            inputName: OPTIONS.inputName || PROP,
            options: OPTIONS.options,
            filters: OPTIONS.filters,
            validators: OPTIONS.validators
          });

          FORM.inputs.push(FORM_INPUT);
        }
      });

    return FORM;
  }
}
