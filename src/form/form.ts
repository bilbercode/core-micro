import { IInput, EInputSource } from "./input/input";
import { Response } from "./input/response";
import { Context } from "../request/context";




export class Form {


  public inputs: Array<IInput> = [];
  private boundInstance: any;

  constructor(boundInstance: any) {
    this.boundInstance = boundInstance;
  }


  isValid(requestContext: any): Promise<boolean|any> {

    let inputContext: any = {};

    this.inputs.forEach(INPUT => {
      inputContext[INPUT.property] = this.getContext(
        requestContext,
        INPUT.source,
        INPUT.inputName
      );
    });

    const VALIDATION_CHAIN = this.inputs
      .map(INPUT => INPUT.isValid(
        this.getContext(requestContext, INPUT.source, INPUT.inputName),
        inputContext
      ));

    return Promise.all(VALIDATION_CHAIN)
      .then(RESULTS => {
        let valid = true;
        let messages: any = {};

        RESULTS.forEach((RESULT: Response) => {
          if (!RESULT.isValid) {
            valid = false;
            messages[RESULT.input.inputName] = RESULT.messages;
          }
        });

        if (valid) {
          RESULTS.forEach(RESULT => {
            this.boundInstance[RESULT.input.property] = RESULT.clean;
          });
          return true;
        }

        return messages;
      });
  }

  public getObject<T>(): T {
    return this.boundInstance;
  }

  private getContext(requestContext: Context, inputContext: EInputSource, inputName: string): any {

    switch (inputContext) {
      case EInputSource.bodyParam:
        return requestContext.getBodyParam(inputName);
      case EInputSource.propertyParam:
        return requestContext.getPropertyParam(inputName);
      default:
        return requestContext.getHeaderParam(inputName);
    }
  }
}
