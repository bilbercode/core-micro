

import { ServiceManager } from "../service/manager";
import { Method } from "./method";
import { Channel } from "amqplib";
import Bluebird = require("bluebird");
import { IQueue } from "../amqp/queue";
import { EventEmitter } from "events";
import { isPromise } from "rxjs/util/isPromise";


export class Controller extends EventEmitter {

  public readonly queue: IQueue;

  private serviceLocator: ServiceManager;

  private methods: Array<Method>;

  constructor(serviceLocator: ServiceManager,
              queue: IQueue,
              methods: Array<Method>) {
    super();
    this.serviceLocator = serviceLocator;
    this.queue = this.serviceLocator.get<IQueue>(queue);
    this.methods = methods;
  }


  init(channel: Channel): Promise<any> {

    const QUEUE_BINDINGS = this.methods.map(METHOD => {
      if (METHOD.binding.exchange) {
        return channel.bindQueue(
          this.queue.name,
          METHOD.binding.exchange.name,
          METHOD.binding.routingKey,
          METHOD.binding.args
        );
      }
    });

    return Promise.all(QUEUE_BINDINGS)
      .then(() => {
        return channel.consume(this.queue.name, MSG => {

          const ROUTE = MSG.fields.routingKey;

          const MATCHES = [];


          this.methods.forEach(METHOD => {
            const ROUTE_PARTS = METHOD.route.split(".");

            let weight = ROUTE_PARTS.length;

            const regStr = ROUTE_PARTS.map(P => {
              if (P === "#") {
                weight --;
                return ".*";
              }
              if (P === "*") {
                weight--;
                return "\\w+";
              }
              return P;
            }).join("\\.");

            const REGEXP = new RegExp(`^${regStr}$`);

            if (!!ROUTE.match(REGEXP)) {
              MATCHES.push({
                method: METHOD,
                weight: weight
              });
            }
          });

          if (MATCHES.length > 0) {

            const MATCH = MATCHES
              .sort((A, B) => {
                if (A.weight === B.weight) return 0;
                return A.weight > B.weight ? -1 : 1;
              })[0];

            let OUTCOME;
            try {
              OUTCOME =  MATCH.method.dispatch.call(MATCH.method, MSG);
            } catch (e) {
              return channel.reject(MSG, false);
            }

            if (isPromise(OUTCOME)) {
              OUTCOME.then(RESPONSE => {
                  channel.ack(MSG);
                  if (MSG.properties.replyTo) {
                    channel
                      .sendToQueue(MSG.properties.replyTo, new Buffer(JSON.stringify(RESPONSE)), {
                      contentType: "application/json",
                      contentEncoding: "utf8",
                      type: "response.success",
                        messageId: MSG.properties.messageId || null
                    });
                  }
              })
              .catch(E => {
                channel.sendToQueue(MSG.properties.replyTo, new Buffer(JSON.stringify(E)), {
                  contentType: "application/json",
                  contentEncoding: "utf8",
                  type: "response.error",
                  messageId: MSG.properties.messageId || null
                });
              });
            } else {
              channel.ack(MSG);
              if (MSG.properties.replyTo) {
                channel.sendToQueue(MSG.properties.replyTo, new Buffer(JSON.stringify(OUTCOME)), {
                  contentType: "application/json",
                  contentEncoding: "utf8",
                  type: "response.success",
                  messageId: MSG.properties.messageId || null
                });
              }
            }

          }
        });
      });
  }

}
