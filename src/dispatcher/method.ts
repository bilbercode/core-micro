import { Message } from "../request/message";
import { Channel } from "amqplib";
import { IQueueBinding } from "../amqp/queue";
import { ServiceManager } from "../service/manager";
import { Context as RequestContext } from "../request/context";
import { Context } from "../request/argument/context";
import { Form } from "../request/argument/form";
import { Metadata } from "neutrino-meta";
import { ARGUMENT, CONTROLLER_ROUTE } from "../metadata/constants";
import { Form as FormInstance} from "../form/form";



function isPromise<T>(value: any): value is Promise<T> {
  return !!(value instanceof Promise);
}

function isRequestContext(value: any): value is Context {
  return !!(value instanceof Context);
}

function isRequestForm(value: any): value is Form {
  return !!(value instanceof Form);
}



export class Method {

  public readonly binding: IQueueBinding;

  public readonly route: string;

  private serviceLocator: ServiceManager;

  private controller: Function;

  private method: string;

  private methodParams: Array<Context|Form>;

  constructor(serviceLocator: ServiceManager, controller: Function, method: string) {
    this.serviceLocator = serviceLocator;
    this.controller = controller;
    this.method = method;
    this.methodParams =
      Metadata.has(ARGUMENT, this.controller, this.method) ?
        Metadata.get<Array<Context|Form>>(ARGUMENT, this.controller, this.method) : [];

    this.binding = Metadata.get<IQueueBinding>(CONTROLLER_ROUTE, this.controller, this.method);
    if (this.binding.exchange) {
      (<any>this).binding.exchange = this.serviceLocator.get(this.binding.exchange);
    }

    this.route = this.binding.routingKey;
  }

  dispatch(message: Message): Promise<any> {

    const CONTEXT = this.serviceLocator.get<RequestContext>(RequestContext, message);

    return this.resolveMethodArguments(CONTEXT)
      .then(PARAMS => {

        const CONTROLLER = this.serviceLocator.get(this.controller);

        return CONTROLLER[this.method](...PARAMS);
      });
  }

  private resolveMethodArguments(context: RequestContext): Promise<any> {
    return Promise.resolve(this.methodParams)
      .then(PARAMS => {
        const PARAMS_ASYNC = PARAMS.map(PARAM => {
          return Promise.resolve<Context|Form>(PARAM)
            .then(P => {
              if (isRequestForm(P)) {
                const MODEL_PROTOTYPE = P.model;
                const FORM: FormInstance = this.serviceLocator
                  .get<FormInstance>(MODEL_PROTOTYPE);
                return FORM.isValid(context)
                  .then(OUTCOME => {
                    if (OUTCOME === true) {
                      return FORM.getObject();
                    }

                    return Promise.reject(OUTCOME);
                  });
              }
              if (isRequestContext(P)) {
                switch (P.name) {
                  case "context":
                    return context;
                  case "body":
                    return context.getBodyParam(P.expression);
                  case "timer":
                    return context.getTimer();
                  case "header":
                    return context.getHeaderParam(P.expression);
                  case "property":
                    return context.getPropertyParam(P.expression);
                  default:
                    // WTF is this? cant do anything with that
                    return null;

                }
              }

              const CONTROLLER_NAME = this.controller.
                prototype.constructor.name;

              throw new Error("Unknown request argument in " +
                `"${CONTROLLER_NAME}::${this.method}" - ` + P);
            });
        });

        return Promise.all(PARAMS_ASYNC);
      });
  }
}
