import { Message } from "./message";
import { Timer } from "./timer/timer";
import { isNullOrUndefined } from "util";

export class Context {

  public readonly message: Message;

  private timer: Timer;


  constructor(message: Message) {
    this.message = message;
    this.timer = new Timer();
  }

  getBodyParam(param: string): string {
    return this.eval(JSON.parse(this.message.content.toString("utf8")), param);
  }

  getHeaderParam(param: string): string {
    return this.eval(this.message.properties.headers, param);
  }

  getPropertyParam(param: string): string {
    return this.eval(this.message.properties, param);
  }

  getTimer(): Timer {
    return this.timer;
  }


  private eval(object: any, expression: string): any {
    let clone = object;

    let pointers = expression.split(/\./g);

    while ((clone = clone[pointers.shift()]) && pointers.length);

    const VALUE = isNullOrUndefined(clone) ? null : clone;

    return JSON.parse(JSON.stringify(VALUE));
  }
}
