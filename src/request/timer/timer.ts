

import { Event } from "./event";
export class Timer {

  public start: Date;

  public events: Array<Event> = [];

  constructor() {
    this.start = new Date();
  }

  mark(event: string) {
    this.events.push(new Event(
      event,
      (new Date()).getTime() - this.start.getTime()
    ));
  }
}
