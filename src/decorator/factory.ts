import { FACTORY, FACTORY_OPTIONS } from "../metadata/constants";
import { Metadata } from "neutrino-meta";

export interface IFactoryOptions {
  exported?: boolean;
  shared?: boolean;
}

export function Factory(target: any, options?: IFactoryOptions): Function {

  return (factory: any) => {

    if (("createService" in factory.prototype) === false) {
      throw new Error("@Factory decorator can only be declared on a class that implements a" +
        " createService method!");
    }


    Metadata.set(FACTORY, target, factory);

    if (options) {
      Metadata.set(FACTORY_OPTIONS, options, factory);
    }

  };
}
