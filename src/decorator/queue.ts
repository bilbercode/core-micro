import { AMQP_QUEUE, INJECTABLE } from "../metadata/constants";
import { Metadata } from "neutrino-meta";

export function Queue(): Function {

  return (target) => {

    if (Metadata.has(INJECTABLE, target)) {
      throw new Error("@Injectable can only be declared once per class");
    }

    Metadata.set(INJECTABLE, { shared: true }, target);
    Metadata.set(AMQP_QUEUE, true, target);
  };
}
