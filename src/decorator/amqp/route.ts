

import { Metadata } from "neutrino-meta";
import { CONTROLLER_ROUTE } from "../../metadata/constants";
import { IQueueBinding } from "../../amqp/queue";

export function Route(options: IQueueBinding): Function {


  return <T>(target: any,
             method: string|symbol,
             descriptor: TypedPropertyDescriptor<T>): TypedPropertyDescriptor<T> => {

    Metadata.set(CONTROLLER_ROUTE, options, target, method);

    return descriptor;
  };
}
