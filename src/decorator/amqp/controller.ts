
import { Metadata } from "neutrino-meta";
import { CONTROLLER } from "../../metadata/constants";
import { IQueue } from "../../amqp/queue";




export function Controller(options: IQueue): Function {

  return (target: any) => {
    Metadata.set(CONTROLLER, options, target);
  };
}
