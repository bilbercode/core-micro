import { Injectable as InjectorDecorator } from "./injectable";
import { Factory as FactoryDecorator } from "./factory";
import { Form as FormDecorator } from "./form";
import * as ArgumentDecorators from "./argument";
import { Input as InputDecorator } from "./input";
import { Queue as QueueDecorator } from "./queue";
import { Exchange as ExchangeDecorator } from "./exchange";
import * as AmqpModule from "./amqp";

/* tslint:disable */
export namespace Decorator {

  export const Controller: Function = AmqpModule.Controller;

  export const Route: Function = AmqpModule.Route;

  export const Injectable: Function = InjectorDecorator;

  export const Factory: Function = FactoryDecorator;

  export const Form: any = FormDecorator;

  export const Argument: any = ArgumentDecorators;

  export const Input: any = InputDecorator;

  export const Queue: any = QueueDecorator;

  export const Exchange: any = ExchangeDecorator;
}
/* tslint:enable */

