import { INPUT, FORM_INPUT } from "../metadata/constants";
import { IInput } from "../form/input/input";
import { isFunction, isNullOrUndefined } from "util";
import { EInputSource } from "../form/input/input";
import { IFilter } from "../form/input/filter/filter";
import { IValidator } from "../form/input/validator/validator";
import { Metadata } from "neutrino-meta";

function isInput(value: any): value is IInput {
  if (isNullOrUndefined(value)) return false;
  return isFunction(value);
}

export interface IInputOptions {
  source: EInputSource;
  options?: any;
  inputName?: string|number;
  validators?: Array<IValidator|Function>;
  filters?: Array<IFilter|Function>;
};

export function Input(options: IInputOptions): Function {

  return <T>(target: any, propertyKey: string,
             descriptor: TypedPropertyDescriptor<T>): TypedPropertyDescriptor<T> => {

    if (Metadata.has(INPUT, target, propertyKey)) {
      throw new Error("@Input() decorator can only be declared once per property");
    }

    Metadata.set(INPUT, options, target, propertyKey);

    const FORM_INPUTS = Metadata.has(FORM_INPUT, target) ?
      Metadata.get<Array<any>>(FORM_INPUT, target) : [];

    FORM_INPUTS.push(propertyKey);

    Metadata.set(FORM_INPUT, FORM_INPUTS, target);


    return descriptor;
  };
}
