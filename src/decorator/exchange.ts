import { AMQP_EXCHANGE, INJECTABLE } from "../metadata/constants";
import { Metadata } from "neutrino-meta";

export function Exchange(): Function {

  return (target) => {

    if (Metadata.has(INJECTABLE, target)) {
      throw new Error("@Injectable can only be declared once per class");
    }

    Metadata.set(INJECTABLE, { shared: true }, target);
    Metadata.set(AMQP_EXCHANGE, true, target);
  };
}
