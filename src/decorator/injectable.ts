import { INJECTABLE } from "../metadata/constants";
import { Metadata } from "neutrino-meta";

export interface IInjectableOptions {
  shared?: boolean;
}

export function Injectable(options?: IInjectableOptions): Function {

  return (target) => {

    if (Metadata.has(INJECTABLE, target)) {
      throw new Error("@Injectable can only be declared once per class");
    }

    if (!options) {
      options = {
        shared: false
      };
    }

    Metadata.set(INJECTABLE, options, target);
  };
}
