import { FORM } from "../metadata/constants";
import { Metadata } from "neutrino-meta";

export function Form(): Function {

  return (target: any) => {

    if (Metadata.has(FORM, target)) {
      throw new Error("@Form decorator can only be declared once per class");
    }

    Metadata.set(FORM, true, target);
  };
}
