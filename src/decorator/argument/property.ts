import { requestContext } from "./request-context";
import { ARGUMENT_PROPERTY } from "../../metadata/constants";

export function Property(expression: string): Function {
  return requestContext(ARGUMENT_PROPERTY, expression);
}
