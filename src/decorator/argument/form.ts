import { ARGUMENT } from "../../metadata/constants";
import { Form as FormArgument } from "../../request/argument/form";
import { Metadata } from "neutrino-meta";


export function Form(form): Function {

  return (target: any, method: string, i: number) => {

    const FUNCTION_ARGUMENTS = Metadata.has(ARGUMENT, target, method) ?
      Metadata.get(ARGUMENT, target, method) : [];

    FUNCTION_ARGUMENTS[i] = new FormArgument(form);

    Metadata.set(ARGUMENT, FUNCTION_ARGUMENTS, target, method);
  };
}
