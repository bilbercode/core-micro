import { ARGUMENT } from "../../metadata/constants";
import { Context } from "../../request/argument/context";
import { Metadata } from "neutrino-meta";

export function requestContext(context: symbol, expression = null): Function {
  return (target: any, method: string, i: number) => {

    const FUNCTION_ARGUMENTS = Metadata.has(ARGUMENT, target, method) ?
      Metadata.get(ARGUMENT, target, method) : [];

    FUNCTION_ARGUMENTS[i] = new Context(context, expression);

    Metadata.set(ARGUMENT, FUNCTION_ARGUMENTS, target, method);
  };
}
