import { requestContext } from "./request-context";
import { ARGUMENT_HEADER } from "../../metadata/constants";

export function Header(expression: string): Function {
  return requestContext(ARGUMENT_HEADER, expression);
}
