import { EventEmitter } from "events";
import { inherits } from "util";
import { Metadata } from "neutrino-meta";
import { DESIGN_PARAM_TYPES, DI_INJECTABLE } from "../metadata/constants";
import { ServiceManager } from "../service/manager";
import { Channel, Connection } from "amqplib";
import { IModule } from "./module";
import { IExchange } from "../amqp/exchange";
import Bluebird = require("bluebird");
import { RABBIT_MQ_CLIENT } from "../amqp/factory/amqp";
import { ModuleFactory } from "./factory/module";

export interface IMicroServiceOptions {
  name: string;
  modules: Array<Function>;
}

export interface ILifecycleMicroService {
  $onInit?(): Promise<any>;
  $onReady?(): Promise<any>;
  $onError?(): Promise<any>;
}

export interface IMicroService {
  start(): Promise<any>;
}

export interface IMicroServiceConfiguration {
  host: string;
  port: number;
  username: string;
  password: string;
  virtualhost: string;
}

export function MicroService(options: IMicroServiceOptions): Function {

  return (target: any) => {

    function ModuleClassDecorator<T extends {new (...args: any[]): {}}>(constructor: T) {

      const NEW_CLASS = class Module extends constructor {

        private serviceLocator: ServiceManager;

        private connection: Connection;

        private channel: Channel;

        private modules: Array<IModule>;

        constructor(...args: any[]) {
          super(...args.slice(1));
          this.serviceLocator = (<ServiceManager>args[0]);

          (<any>this).on("error", (e, context) => {
            if ("$onError" in this) {
              (<any>this).$onError(e, context);
            }
          });
        }

        start(config: IMicroServiceConfiguration): Promise<any> {

          return Promise.resolve()
            .then(() => "$onInit" in this ? (<ILifecycleMicroService>this)
              .$onInit.call(this) : null)
            .then(() => this.createModules())
            .then(() => this.initModuleProviders())
            .then(() => this.initModuleControllers())
            .then(() => this.initChannel(config))
            .then(() => this.initExchanges())
            .then(() => this.initQueues())
            .then(() => this.startControllers())
            .then(() => "$onReady" in this ? (<ILifecycleMicroService>this)
              .$onReady.call(this) : null)
            .catch((e) => (<any>this).emit("error", e));
        }

        private createModules(): Promise<any> {


          this.modules = options.modules.map<IModule>(MODULE => {
            this.serviceLocator.registerFactory(MODULE, ModuleFactory);

            return this.serviceLocator.get<IModule>(MODULE);
          });

          return Promise.resolve();
        }

        private initModuleProviders(): Promise<any> {

          return Promise.all(this.modules.map(MODULE => MODULE.initProviders()));
        }

        private initModuleControllers(): Promise<any> {
          return Promise.all(this.modules.map(MODULE => MODULE.initControllers()));
        }

        private initChannel(config: IMicroServiceConfiguration): Bluebird<any> {

          const PROMISE = this.serviceLocator.get<Bluebird<Connection>>(RABBIT_MQ_CLIENT, config);

          return PROMISE.then(CONNECTION => {
            this.connection = CONNECTION;

            return CONNECTION.createChannel()
              .then(CHANNEL => {
                this.channel = CHANNEL;
                return this.channel;
              });
          });
        }


        private initExchanges(): Promise<any> {

          const EXCHANGES_INIT: Array<Bluebird<IExchange>|Promise<IExchange>> = [];

          this.modules.forEach(MODULE => {
            MODULE.exchanges.map(EXCHANGE => {
              EXCHANGES_INIT.push(this.channel
                .assertExchange(EXCHANGE.name, EXCHANGE.type, EXCHANGE.options)
                .then(() => {
                  return EXCHANGE;
                }));
            });
          });

          return Promise.all(EXCHANGES_INIT)
            .then(EXCHANGES => {
              return Promise.all(EXCHANGES.map(EXCHANGE => {
                if (EXCHANGE.bindings && EXCHANGE.bindings.length > 0) {
                  return Promise.all(EXCHANGE.bindings.map(BINDING => {
                    this.channel
                      .bindExchange(
                        BINDING.destination.name,
                        EXCHANGE.name,
                        BINDING.routingKey,
                        BINDING.args);
                  }));
                }
              }));
            });
        }

        private initQueues(): Promise<any> {
          return Promise.all(this.modules.map(MODULE => {
            return Promise.all(MODULE.queues.map(QUEUE => {
              return this.channel.assertQueue(QUEUE.name, QUEUE.options);
            }));
          }));
        }

        private startControllers(): Promise<any> {
          return Promise.all(this.modules.map(MODULE => {
            return Promise.all(MODULE.controllers.map(CONTROLLER => {
              return CONTROLLER.init(this.channel);
            }));
          }));
        }

      };


      const PARAMS = Metadata.has(DESIGN_PARAM_TYPES, target) ?
        Metadata.get<Array<any>>(DESIGN_PARAM_TYPES, target) : [];

      PARAMS.unshift(ServiceManager);

      Metadata.set(DI_INJECTABLE, PARAMS, NEW_CLASS);

      return NEW_CLASS;
    };


    function decorateEventEmitter<T extends {new (...args: any[]): {}}>(constructor: T) {

      const ORIGIN = constructor.constructor;

      constructor.constructor = function (...args: any[]) {
        ORIGIN.apply(this, args);
        EventEmitter.call(this);
      };

      inherits(constructor, EventEmitter);

      return constructor;
    }

    return ModuleClassDecorator(decorateEventEmitter(target));
  };
}
