import "source-map-support/register";
import { ServiceManager } from "../service/manager";
import {
  CONTROLLER, FACTORY, FACTORY_OPTIONS,
  INJECTABLE, DESIGN_PARAM_TYPES, DI_INJECTABLE,
  APPLICATION_MODULE, FORM, AMQP_EXCHANGE, AMQP_QUEUE
} from "../metadata/constants";

import { isNullOrUndefined } from "util";
import { WhitelistedServiceManager } from "../service/whitelisted-manager";
import { EventEmitter } from "events";
import * as Util from "util";
import { FormFactory } from "../form/factory/form";
import { Metadata } from "neutrino-meta";
import { Controller } from "../dispatcher/controller";
import { IExchange } from "../amqp/exchange";
import { IQueue } from "../amqp/queue";

export interface IModuleOptions {
  name: string;
  controllers?: Array<Function>;
  providers?: Array<Function>;
  export?: Array<Function|symbol>;
  import?: Array<any>;
  __id?: symbol;
}

export interface IModule {
  name: string;
  controllers: Array<Controller>;
  export: Array<Function|symbol>;
  exchanges: Array<IExchange>;
  queues: Array<IQueue>;
  initProviders(): Promise<any>;
  initControllers(): Promise<any>;
}


export function Module(options: IModuleOptions): Function {

  return (target: any) => {

    let exportedServiceLocator: WhitelistedServiceManager =
      new WhitelistedServiceManager(Metadata);

    function ModuleClassDecorator<T extends {new (...args: any[]): {}}>(constructor: T) {

      const NEW_CLASS = class Module extends constructor {

        public name: string = options.name;

        public controllers: Array<Controller> = [];

        public import: Array<Module> = options.import;

        public providers: Array<Function> = options.providers;

        public exchanges: Array<IExchange> = [];

        public queues: Array<IQueue> = [];

        static get exportedServiceLocator(): WhitelistedServiceManager {
          return exportedServiceLocator;
        }

        static get export(): Array<Function|symbol> {
          return options.export;
        }

        get export(): Array<Function|symbol> {
          return options.export;
        }

        private set exportedServiceLocator(serviceLocator: WhitelistedServiceManager) {
          exportedServiceLocator = serviceLocator;
        }

        private get exportedServiceLocator(): WhitelistedServiceManager {
          return exportedServiceLocator;
        }


        private serviceLocator: ServiceManager;

        private meta: Metadata;

        constructor(...args: any[]) {
          super(...args.slice(2));
          this.serviceLocator = (<ServiceManager>args[0]);
          this.meta = (<Metadata>args[1]);
          this.exportedServiceLocator.setWhitelist(this.export);
          this.exportedServiceLocator.setPeeringManager(this.serviceLocator);
        }

        initControllers(): Promise<Module> {

          if (options.controllers) {
            options.controllers
              .forEach(COMPONENT => {
                /**
                 * Register controllers with their respective dispatcher
                 */
                if (this.meta.has(CONTROLLER, COMPONENT)) {

                  const PARAMS = this.meta.has(DESIGN_PARAM_TYPES, COMPONENT) ?
                    this.meta.get(DESIGN_PARAM_TYPES, COMPONENT) : [];

                  this.meta.set(DI_INJECTABLE, PARAMS, COMPONENT, this.serviceLocator.id);

                  const CONTROLLER_INSTANCE = this.serviceLocator
                    .get<Controller>(Controller, {
                      dispatchable: COMPONENT,
                      queue: Metadata.get(CONTROLLER, COMPONENT)
                    });
                  this.serviceLocator.setShared(COMPONENT, true);

                  CONTROLLER_INSTANCE.on("error", (msg) => {
                    (<any>this).emit("error", msg);
                  });

                  this.controllers.push(CONTROLLER_INSTANCE);
                }
              });
          }
          return Promise.resolve(this);
        }

        initProviders(): Promise<Module> {

          if (this.import) {
            this.import.forEach(MODULE => {
              this.serviceLocator.addPeeringManager(MODULE.exportedServiceLocator);
            });
          }


          if (this.providers) {
            this.providers
              .forEach(COMPONENT => {
                if (this.meta.has(FORM, COMPONENT)) {
                  const PARAMS = this.meta.has(DESIGN_PARAM_TYPES, COMPONENT) ?
                    this.meta.get(DESIGN_PARAM_TYPES, COMPONENT) : [];

                  this.meta.set(DI_INJECTABLE, PARAMS, COMPONENT, this.serviceLocator.id);
                  this.serviceLocator.registerFactory(COMPONENT, FormFactory);
                }

                if (this.meta.has(FACTORY, COMPONENT)) {

                  const PARAMS = this.meta.has(DESIGN_PARAM_TYPES, COMPONENT) ?
                    this.meta.get(DESIGN_PARAM_TYPES, COMPONENT) : [];

                  this.meta.set(DI_INJECTABLE, PARAMS, COMPONENT, this.serviceLocator.id);

                  const FACTORY_CONFIG = this.meta.has(FACTORY_OPTIONS, COMPONENT) ?
                    this.meta.get<any>(FACTORY_OPTIONS, COMPONENT) : {
                      shared: false,
                      exported: false
                    };

                  const SERVICE_LOCATOR = this.serviceLocator;

                  SERVICE_LOCATOR
                    .registerFactory(this.meta.get(FACTORY, COMPONENT), COMPONENT);

                  SERVICE_LOCATOR.setShared(
                    this.meta.get<any>(FACTORY, COMPONENT), FACTORY_CONFIG.shared || false);
                }

                if (this.meta.has(INJECTABLE, COMPONENT)) {

                  const INJ_OPT = this.meta.get<{ shared: boolean }>(INJECTABLE, COMPONENT);
                  const PARAMS = this.meta.has(DESIGN_PARAM_TYPES, COMPONENT) ?
                    this.meta.get(DESIGN_PARAM_TYPES, COMPONENT) : [];
                  this.meta.set(DI_INJECTABLE, PARAMS, COMPONENT, this.serviceLocator.id);

                  if (INJ_OPT.shared) {
                    this.serviceLocator.setShared(COMPONENT, true);
                  }
                }

                if (this.meta.has(AMQP_EXCHANGE, COMPONENT)) {
                  this.exchanges.push(new (<any>COMPONENT)());
                }

                if (this.meta.has(AMQP_QUEUE, COMPONENT)) {
                  this.queues.push(new (<any>COMPONENT)());
                }

              });
          }
          return Promise.resolve(this);
        }
      };


      Metadata.set(INJECTABLE, options, target);

      const PARAMS = Metadata.has(DESIGN_PARAM_TYPES, target) ?
        Metadata.get<Array<any>>(DESIGN_PARAM_TYPES, target) : [];

      PARAMS.unshift(Metadata);
      PARAMS.unshift(ServiceManager);

      Metadata.set(DI_INJECTABLE, PARAMS, target);
      Metadata.set(APPLICATION_MODULE, target, NEW_CLASS);

      return NEW_CLASS;
    }

    function decorateEventEmitter<T extends {new (...args: any[]): {}}>(constructor: T) {

      const ORIGIN = constructor.constructor;

      constructor.constructor = function (...args: any[]) {
        ORIGIN.apply(this, args);
        EventEmitter.call(this);
      };

      Util.inherits(constructor, EventEmitter);

      return constructor;
    }

    return ModuleClassDecorator(decorateEventEmitter(target));
  };
}

