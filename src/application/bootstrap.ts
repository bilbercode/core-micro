import { ServiceManager } from "../service/manager";
import { ApplicationFactory } from "./factory/application";
import { DI_INJECTABLE } from "../metadata/constants";
import { IMicroService } from "./microservice";
import { Metadata } from "neutrino-meta";

export function microServiceBootstrap(application): IMicroService {

  const APPLICATION_SERVICE_MANAGER = new ServiceManager(Metadata);

  Metadata.set(DI_INJECTABLE, [], ApplicationFactory, APPLICATION_SERVICE_MANAGER.id);

  APPLICATION_SERVICE_MANAGER.registerFactory(application, ApplicationFactory);

  return APPLICATION_SERVICE_MANAGER.get<IMicroService>(application);
}
