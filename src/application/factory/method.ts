

import { IFactory } from "../../service/factory";
import { ServiceManager } from "../../service/manager";
import { Method } from "../../dispatcher/method";

interface MethodFactoryOptions {
  dispatchable: Function;
  method: string;
}

export class MethodFactory implements IFactory {

  createService<T>(serviceLocator: ServiceManager, options: MethodFactoryOptions, constructor?: Function): Method {

    return new Method(serviceLocator, options.dispatchable, options.method);
  }
}
