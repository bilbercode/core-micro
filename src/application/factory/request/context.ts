
import { Message } from "../../../request/message";
import { IFactory } from "../../../service/factory";
import { ServiceManager } from "../../../service/manager";
import { Context } from "../../../request/context";


export class ContextFactory implements IFactory {

  createService<T>(serviceLocator: ServiceManager, message: Message, constructor?: Function): Context {
    return new Context(message);
  }
}
