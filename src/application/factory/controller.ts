import { IFactory } from "../../service/factory";
import { ServiceManager } from "../../service/manager";
import { IQueue } from "../../amqp/queue";
import { Metadata } from "neutrino-meta";
import { CONTROLLER_ROUTE } from "../../metadata/constants";
import { Method } from "../../dispatcher/method";

interface IControllerFactoryOptions {
  dispatchable: any;
  queue: IQueue;
};

export class ControllerFactory implements IFactory {

  createService<T>(serviceLocator: ServiceManager, options: IControllerFactoryOptions, constructor?: Function): T {

    const METHODS = []; // TODO methods from options.dispachable

    Object.getOwnPropertyNames(options.dispatchable.prototype)
      .forEach(METHOD => {
        if (Metadata.has(CONTROLLER_ROUTE, options.dispatchable, METHOD)) {
          METHODS.push(serviceLocator.get<Method>(Method, {
            dispatchable: options.dispatchable,
            method: METHOD
          }));
        }
      });
    return new (<any>constructor)(serviceLocator, options.queue, METHODS);
  }
}
