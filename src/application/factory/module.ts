import { IFactory } from "../../service/factory";
import { ServiceManager } from "../../service/manager";
import { Injector } from "../../di/injector";
import {
  FACTORY, FACTORY_OPTIONS, APPLICATION_MODULE,
  DI_INJECTABLE, MODULE_SERVICE_LOCATOR
} from "../../metadata/constants";
import { Metadata } from "neutrino-meta";

export class ModuleFactory implements IFactory {

  createService(serviceLocator: ServiceManager,
                options?: any,
                target?: FunctionConstructor): any {

    const SERVICE_MANAGER = serviceLocator.get<ServiceManager>(MODULE_SERVICE_LOCATOR);

    Metadata.set(DI_INJECTABLE, [], Metadata, SERVICE_MANAGER.id);



    const METADATA = Metadata;

    const DEPENDENCY_INJECTOR = new Injector(SERVICE_MANAGER, METADATA);

    if ((<any>target).providers) {

      (<any>target).providers.forEach(COMPONENT => {
        if (METADATA.has(FACTORY, COMPONENT)) {

          const FACTORY_CONFIG = METADATA.has(FACTORY_OPTIONS, COMPONENT) ?
            METADATA.get<any>(FACTORY_OPTIONS, COMPONENT) : { shared: false, exported: false};

          SERVICE_MANAGER
            .registerFactory(METADATA.get(FACTORY, COMPONENT), COMPONENT);

          SERVICE_MANAGER.setShared(
            METADATA.get<any>(FACTORY, COMPONENT), FACTORY_CONFIG.shared || false);
        }
      });
    }

    const TARGET = METADATA.get<any>(APPLICATION_MODULE, target);

    const INJ_ARGS = METADATA.has(DI_INJECTABLE, TARGET) ?
      METADATA.get<Array<any>>(DI_INJECTABLE, TARGET) : [];

    const INSTANCE_ARGS = INJ_ARGS.map((dep, i) => {
      switch (i) {
        case 0:
          return SERVICE_MANAGER;
        case 1:
          return METADATA;
        default:
          return SERVICE_MANAGER.get.call(SERVICE_MANAGER, dep, null);
      }
    });

    const INSTANCE = new target(...INSTANCE_ARGS);

    return INSTANCE;
  }
}
