import { IFactory } from "../../../service/factory";
import { ServiceManager } from "../../../service/manager";


import { WhitelistServiceLocatorFactory } from "./whitelist";
import { WhitelistedServiceManager } from "../../../service/whitelisted-manager";



import { DI_INJECTABLE, DESIGN_PARAM_TYPES } from "../../../metadata/constants";


import { TrimString } from "../../../form/input/filter/trim-string";
import { StripTags } from "../../../form/input/filter/strip-tags";
import { NotEmpty } from "../../../form/input/validator/not-empty";
import { Form } from "../../../form/form";
import { FormFactory } from "../../../form/factory/form";
import { Input } from "../../../form/input/input";
import { InputFactory } from "../../../form/factory/input";
import { Metadata } from "neutrino-meta";
import { Controller } from "../../../dispatcher/controller";
import { ControllerFactory } from "../controller";
import { Method } from "../../../dispatcher/method";
import { MethodFactory } from "../method";
import { Context } from "../../../request/context";
import { ContextFactory } from "../request/context";




export class ModuleServiceLocatorFactory implements IFactory {

  createService(serviceLocator: ServiceManager, options?: any): ServiceManager {

    const SERVICE_LOCATOR = new ServiceManager(Metadata);

    SERVICE_LOCATOR.registerFactory(Form, FormFactory);
    SERVICE_LOCATOR.registerFactory(Input, InputFactory);
    SERVICE_LOCATOR.registerFactory(Controller, ControllerFactory);
    SERVICE_LOCATOR.registerFactory(Method, MethodFactory);
    SERVICE_LOCATOR.registerFactory(Context, ContextFactory);

    SERVICE_LOCATOR.registerFactory(WhitelistedServiceManager, WhitelistServiceLocatorFactory);


    const INJECTABLES = [
      TrimString,
      StripTags,
      NotEmpty,
      WhitelistServiceLocatorFactory,
      FormFactory,
      InputFactory,
      ControllerFactory,
      MethodFactory,
      ContextFactory,
    ];

    INJECTABLES.forEach(INJECTABLE => {
      this.registerInjectable(INJECTABLE, SERVICE_LOCATOR.id);
    });

    SERVICE_LOCATOR.set(Metadata, Metadata);

    return SERVICE_LOCATOR;
  }


  private registerInjectable(injectable: Function,
                             slID: symbol) {

    const PARAMS = Metadata.has(DESIGN_PARAM_TYPES, injectable) ?
      Metadata.get(DESIGN_PARAM_TYPES, injectable) : [];

    Metadata.set(
      DI_INJECTABLE,
      PARAMS,
      injectable,
      slID
    );
  }
}
