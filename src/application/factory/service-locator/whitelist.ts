import { IFactory } from "../../../service/factory";
import { ServiceManager } from "../../../service/manager";
import { WhitelistedServiceManager } from "../../../service/whitelisted-manager";
import { Metadata } from "neutrino-meta";


export class WhitelistServiceLocatorFactory implements IFactory {

  createService(serviceLocator: ServiceManager,
                   options: Array<Function|FunctionConstructor|symbol>): WhitelistedServiceManager {

    const INSTANCE = new WhitelistedServiceManager(Metadata, options);
    INSTANCE.setPeeringManager(serviceLocator);
    return INSTANCE;
  }
}
