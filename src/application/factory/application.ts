import { IFactory } from "../../service/factory";
import { ServiceManager } from "../../service/manager";
import {
  DI_INJECTABLE, DESIGN_PARAM_TYPES,
  MODULE_SERVICE_LOCATOR
} from "../../metadata/constants";

import { ModuleServiceLocatorFactory } from "./service-locator/module";
import { ModuleFactory } from "./module";
import { Metadata } from "neutrino-meta";
import { AmqpClient, RABBIT_MQ_CLIENT } from "../../amqp/factory/amqp";


export class ApplicationFactory implements IFactory {


  createService(serviceLocator: ServiceManager,
                   options?: any,
                   target?: FunctionConstructor) {

    const APPLICATION_SERVICE_LOCATOR = new ServiceManager(Metadata);

    Metadata.set(DI_INJECTABLE, [], ModuleServiceLocatorFactory, APPLICATION_SERVICE_LOCATOR.id);
    Metadata.set(DI_INJECTABLE, [], ModuleFactory, APPLICATION_SERVICE_LOCATOR.id);

    APPLICATION_SERVICE_LOCATOR.registerFactory(
      MODULE_SERVICE_LOCATOR, ModuleServiceLocatorFactory);

    APPLICATION_SERVICE_LOCATOR.registerFactory(
      RABBIT_MQ_CLIENT, AmqpClient
    );

    this.registerInjectable(AmqpClient, APPLICATION_SERVICE_LOCATOR.id);

    const PARAMS: Array<any> = Metadata.get<Array<any>>(DI_INJECTABLE, target);

    const INJECTIONS = PARAMS.map((PARAM, i) => {
        switch (i) {
          case 0:
            return APPLICATION_SERVICE_LOCATOR;
          default:
            return APPLICATION_SERVICE_LOCATOR.get<any>(PARAM);
        }
      });

    return new target(...INJECTIONS);
  }

  private registerInjectable(injectable: Function,
                             slID: symbol) {

    const PARAMS = Metadata.has(DESIGN_PARAM_TYPES, injectable) ?
      Metadata.get(DESIGN_PARAM_TYPES, injectable) : [];

    Metadata.set(
      DI_INJECTABLE,
      PARAMS,
      injectable,
      slID
    );
  }
}
